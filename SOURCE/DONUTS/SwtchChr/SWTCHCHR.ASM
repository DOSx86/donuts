; MIT License
;
; Copyright (c) 2022 Jerome Shidel
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; NASM 2.13.03 or later

use16

cpu 8086

%include 'NASMPLUS.INC'

; Not address specific
; org 0x100
; org 0x0000

; get address of prefix to message and print it
movip   	dx
add		dx, Message - 3
mov		ah, 0x09
int		0x21

; get DOS Switch Character and print it
push		dx
mov		ax, 0x3700
int		0x21
test		al, al
jnz		NotSupported
mov		ah, 0x02
int		0x21
jmp		Displayed
NotSupported:
pop		dx
push		dx
mov		ah, 0x09
add 		dx, Error - Message
int		0x21
Displayed:
pop		dx

; print tail of drive letter message and print it
mov		ah, 0x09
add 		dx, Last - Message
int		0x21

; Return to DOS
; mov		ax, 0x4c00
; int		0x21
ret

Message:
		db 'SWITCH_CHAR=$'
Last:
		db 0x0d, 0x0a, '$'
Error:
		db 'unsupported$'
