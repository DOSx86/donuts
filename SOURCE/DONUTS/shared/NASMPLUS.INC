; MIT License
;
; Copyright (c) 2022 Jerome Shidel
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; NASM 2.13.03 or later

; Load the Current value of the IP into a Register. When used to reference an
; offset in code you will need to subtract 3 bytes for the instruction length
; this macro could do it. However since you will probably be adding or
; subtracting that offset, it wastes bytes to do it here.

%imacro movip 1
	call 		%%ShortCall
%%ShortCall:
	pop	 	%1
%endmacro

%imacro PrintHexByte 1
	mov		al, %1
	xor 		ah, ah
	push		ax
	mov		cl, 0x04
	shr		ax, cl
	call		%%ShowHexDigit
	pop		ax
	push		ax
	call		%%ShowHexDigit
	pop		ax
	jmp		%%Printed

%%ShowHexDigit:
	and		al, 0x0f
	add		al, 0x30
	cmp		al, 0x39
	jbe		%%PrintDigit
	add		al, 0x27
%%PrintDigit:
	mov		dl, al
	mov		ah, 0x02
	int		0x21
	ret
%%Printed:
%endmacro

%imacro PrintHexWord 1
	mov		ax, %1
	push 		ax
	mov		al, ah
	call		%%PrintByte
	pop		ax
	call		%%PrintByte
	jmp		%%Printed
%%PrintByte:
	PrintHexByte 	al
	ret
%%Printed:
%endmacro

%imacro PrintCRLF 0
	mov		ah, 0x02
	mov		dl, 0x0d
	int		0x21
	mov		dl, 0x0a
	int		0x21
%endmacro