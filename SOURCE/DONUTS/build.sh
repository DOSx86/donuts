#!/bin/bash

function build_tool () {
	build_ext=COM
	build_out=../../../BIN

	if [[ ! -d "${build_out}" ]] ; then
		mkdir -p "${build_out}" || exit $?
	fi

	for i in *.ASM *.asm ; {
		[[ ! -f "${i}" ]] && continue
		o=$(tr "[:lower:]" "[:upper:]" <<< "${i%.*}.${build_ext}")
		o="${build_out}/${o}"
		if [[ -f "${o}" ]] ; then
			# ls -ald "${o}"
			rm -f "${o}"
		fi
		nasm "${i}" -I../shared/ -fbin -O9 -o "${o}" || exit 1
		if [[ -f "${o}" ]] ; then
	   		# pushd "${build_out}" >/dev/null || exit $?
			# ls -ald "${o##*/}"
			# popd >/dev/null
			ls -ald "${o}"
		else
			return 1
		fi
	}
	return 0
}

for t in * ; do
	[[ ! -d "${t}" ]] && continue
	pushd "${t}" >/dev/null || exit $?
	if [[ -x "build.sh" ]] ; then
		./build.sh
	else
		build_tool
	fi
	ret=$?
	popd >/dev/null
	[[ ${ret} -ne 0 ]] && exit ${ret}
done

