# Donuts

Disk and Other New Utility Tool Suite for DOS

### BOOTDRV.COM

Returns the drive used to boot the system as reported by DOS. This is going to
be A: or C: (probably).

### DOSOEMID.COM

Returns the DOS OEM ID Number as Hex. For Example, 0xfd is FreeDOS.

### DOSVER.COM

Returns the version of DOS as reported by DOS.

### LASTDRV.COM

Returns the last drive letter in the system as reported by DOS. This will be
either the maximum drive letter found or the LASTDRIVE setting in the system
config file.

### SWTCHCHR.COM
Returns the current command-line switch character used by DOS.

### TESTANSI.COM

Checks for the ANSI escape sequence support. Returns a text regarding the
result. It terminates with errorlevel 1 when support is not detected.

### VERIFLAG.COM

Returns the current state of the DOS write verify flag. (possibly in the
future, add support to enable/disable write verification.)

### VIDKIND.COM

Return video card graphics support kind. SuperVGA, VGA, MCGA, EGA, CGA or MDA.
There are other cards bellow EGA, like InColor, Hercules, etc. But, the utility
makes no effort to test for those at present. I have no way to test the results
for cards less than EGA. So, results would be iffy at best.

