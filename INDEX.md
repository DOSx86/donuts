# Donuts

Disk and Other New Utility Tool Suite for DOS


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DONUTS.LSM

<table>
<tr><td>title</td><td>Donuts</td></tr>
<tr><td>version</td><td>0.11</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-09-15</td></tr>
<tr><td>description</td><td>Disk and Other New Utility Tool Suite for DOS</td></tr>
<tr><td>keywords</td><td>dos 16 bit</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>original&nbsp;site</td><td>https://gitlab.com/DOSx86/donuts</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT License](LICENSE)</td></tr>
</table>
